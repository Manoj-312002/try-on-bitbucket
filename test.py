print('hello world')

class person:
    x = 45
    def varchange (self,c):
        self.x = c


m = person()
print(m.x)
print(person.x)

m.x = 50
print(m.x)
print(person.x)

person.x = 55
print(m.x)
print(person.x)

m.varchange(60)
print(m.x)
print(person.x)

class nextiteration:
    def __init__(self,var):
        self.var = var
        self.offset = 0
    def __next__(self):
        if self.offset > len(self.var)-1:
            raise StopIteration                               
        else:
            item = self.var[self.offset]
            self.offset +=1
            return item

class variable():
    def __init__(self,var):
        self.var = var
        self.offset = 0


    def __index__(self):                                       #for bin()   
        return 5
    def __getitem__(self,i):                                   #for slicing   
        return self.var[i]      
    def __setitem__(self,i,value):                             #for slice setting    
        self.var =  self.var[:i] + value +self.var[i+1:]
    def __iter__(self):
        for i in range(len(self.var)):
            yield self.var[i]
            '''return nextiteration(self.var)'''           
#                                                              #for single iteration use self   
#                                                              #return in iter fetches the next of the object   
    def __str__(self):
        return self.var
    '''def __next__(self):
        if self.offset > len(self.var)-1: 
            raise StopIteration                                this is for single iteration   
        else:
            item = self.var[self.offset]
            self.offset +=1
            return item'''

a = variable('Manoj') 
for i in a:
    for j in a:
        print(i+j,end = " ")

a[3] = 's'
print(a)
b = variable(12)
print()

class variabletest():
    b = 5
    _c = 10
    __a = 5
    def __init__(self,value):
        self._d = value
        print('hello')
    def var(self):
        self.__test = 5
    def print(self):
        print(self.__test)
__init__ = 3
x = variabletest(6)
x.var()
print(x)
print(str(variabletest.__bases__).format())
print(variabletest)

#print(x.__test)            there is no attribute test 
print(x._variabletest__test)              #this works because pseudoprivate name can be accessed by this way
print(x._c)
print(variabletest._c)                 #both of the works 
#print(x.c)              this does not work 
#print(x._d)             this also does not work _d can be used in __getattr__ and __setattr__ do there is no recursion 

def ke(m:int,v:int) :
    return m*5 +v*5 + 5
def ke(m:str,v:int):
    return m + str(v *5)

a = person()
print()


for i in range(5):
    for j in range(5):
        if (i*j ) == 0 or (i*j)%4 == 0:print('*',end = '')
        else :print(' ',end = '')
    print()
import math
for i in range(0,361,30):
    t = math.radians(i) 
    a = math.sin(t)
    b = math.cos(t)
    c = math.tan(t)
    print('sin{0:5} = {1:+.2f} \t cos{0:5} = {2:+.2f} \t tan{0:5} = {3:+.2f}'.format('({})'.format(i),a,b,c))
