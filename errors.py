class manoj:
    def __init__(self,func):
        self.calls =0 
        self.func = func

    def getage(self):
        return 40
    #age = property(getage)         # this is especially used when superclass raises error on __getattr__

    def __call__(self,a):
        print(a)
#a = manoj()
#print(a(3))
#@manoj
#def spam(a):
 #   print(a+2)

#spam(3)
'''
a = manoj()
a.name = 4
print(a.age)'''

class var:
    a= [4]

i = var()
i.a.append(5)

print(var.a)
x = 'Mnoj'
def fetcher(i): return x[i]

try:
    fetcher(3)
    print('hi')
except ZeroDivisionError:
    print('nothing')
else:
    print('bye ')

print('Hi')