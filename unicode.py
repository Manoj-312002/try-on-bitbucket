print(b'hi')
s = 'Manojselvam'
print(d:=s.encode())
print(a:=s.encode('utf-16'),len(a))
print(b:=s.encode('utf-32'),len(b))
print(c:=s.encode('latin -1'),len(c))
c = bytes('manoj','utf-8')
c = bytearray(d)               # it could be a utf-8 or a string with encoding mentioned
print(c)
a = b'manoj'

#print(dir(a),a.hex())

from re import *
a = 'The quick brown fox jumps over the lazy dog'
print(match('The (.*)over (.*)',a).groups())

from struct import pack
pack()

import struct


