class tracer:
    def __init__(self,func):
        self.func = func
        self.calls = 0
    def __call__ (self,*args):
        self.calls +=1
        print(self.calls,self.func.__name__)
        return self.func(*args)

@tracer 
def spam (a,b,c):
    return a+b+c

print(spam(1,2,3))


def tracer(func):
    def oncall(*args):
        oncall.calls +=1
        print(oncall.calls,func.__name__)
        return func(*args)
    oncall.calls = 0 
    return oncall
class c:
    @tracer 
    def spam(self,a,b,c):return a+b+c

x = c()
print(x.spam(1,2,3))

def the():
    print('Why do we need git shows')
    

class a:
    def method (self):
        print('a.method');super().method()

class b(a):
    def method (self):
        print('b.method');super().method()

class c(a):
    def method (self):
        print('c.method')
class d(b,c):
    def method(self):
        print('d.method');super().method()

x = d()
x.method()
print(d.__mro__)
