class tracer:
    def __init__(self,func):
        self.calls = 0
        self.func = func
    
    def __call__ (self,*args):
        self.calls +=1 
        print('tracer call',self,*args)
        return self.func(*args)


    def __get__ (self,instance,owner):
        print('get',self,instance,owner)
        return wrapper(self,instance)

class wrapper:
    def __init__(self,desc,subj):
        self.desc = desc
        self.subj = subj
    
    def __call__(self,*args):
        print(self,args,self.desc,self.subj)
        return self.desc(self.subj,*args)


@tracer
def spam(a,b,c):
    print(a,b,c)

spam(1,2,3)

class classfunc:
    @tracer
    def classmethod(self,args):
        print(self,args)


x = classfunc()
x.classmethod(1)
