class decorator:
    def __init__(self,func):
        print(self,func)
        self.func = func

    def __call__(self,arg):
        print('doing nothing',arg)
    
class someclass:
    @decorator
    def function(self,arg):
        print(self,arg)

x = someclass()
x.function(1)

