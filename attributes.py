class attr:
    name = 'manoj'
    _sname = 'selvam'

 
    def __getattr__(self,name):                   #run for undifined attritures
        print('got attr',name)
        return self.name
        
    def __getattribute__(self, name):             #run for defined attributes
        return self._sname

a = attr()
print(a.name)
print(a.sname)                                     #both of them calls getattribute
print(a.undefined)                                 #calls getattr
