for i in range(10):
    print (i)

print ('this is an optional statement')
class manoj(Exception):
    def __init__(self,name):
        self.name = name 

    @staticmethod 
    def name():
        print(manoj)

    @classmethod 
    def hi(cls):
        print(cls)
    
    @staticmethod
    def chumma():
        print('Manoj selvam')


#a = manoj('Manoj')
#manoj.hi()
#a.hi()
#manoj.name()    
#a.name()

def function():
    x = manoj('name')
    raise x
import sys 
try:
    function()
except manoj:
    print('caught',manoj,sys.exc_info())

m = (1,1)
a = [(m:= (m[1],sum(m)))[0] for i in range (10)]
print(a)


class chumma(Exception):
    def __str__(self):
        return 'this is an error statement'
print()
x = chumma()

print(None)
try:
    raise chumma()
except Exception as s:
    print(s)
    print(s.args)


